#!/bin/bash
#
# Create a bootable volume from an image and launch openstack instance
#
# Fabricio Boreli

# Openstack credentials, project id and project name must be supplied as envrironment variables
# 
#  Variables must be sourced from another file
#  E.g 
#  $ bash os_launch_instance vm01

. $1

echo "Project name:         ${OS_PROJECT_NAME}"
echo "Project ID:           ${OS_PROJECT_ID}" && echo
echo "Instance name:        ${instance_name}"
echo "Image name:           ${image_name}"
echo "Availability zone:    ${availability_zone}"
echo "Key name:             ${ssh_key_name}"
echo "Volume size:          ${boot_volume_size}"
echo "Flavor name:          ${flavor_name}"
echo "Volume type:          ${volume_type}"
echo "Internal network ID:  ${network_id}"
echo "Security group:       ${sec_group_name}" && echo
echo "Press any key to continue or ESC key to cancel"

read -r -n1 key
[[ $key == $'\e' ]] &&  exit 4

echo "Creating volume from image ${image_name}."
openstack volume create \
  --type "${volume_type}" \
  --size "${boot_volume_size}" \
  --image "${image_name}" \
  --bootable \
  "${instance_name}-disk01"

# 300 seconds timeout
i=0
while [ "$i" -lt 11 ]
do
  [ "$(openstack volume show ${instance_name}-disk01 -f shell | \
    grep '^status="available"$')" ] && echo "Volume is ready." && break
  ((i++))
  [ "$i" -eq 10 ] && echo "Volume creation timed out, aborting." && exit 1
  sleep 30
done

echo "Launching instance - ${instance_name}."
openstack server create \
  --flavor "${flavor_name}" \
  --volume "${instance_name}-disk01" \
  --network "${network_id}" \
  --security-group "${sec_group_name}" \
  --key-name "${ssh_key_name}" \
  --availability-zone "${availability_zone}" \
  --wait \
  "${instance_name}" || exit 2

exit 0
